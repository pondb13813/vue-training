import Vue from "vue";
import App from "./App.vue";
import "./assets/tailwind.css";
import router from "./router";
import store from "./store";
import dayjs from "dayjs";

Vue.config.productionTip = false;

Vue.prototype.$dayjs = dayjs;
Vue.prototype.$sayHello = function () {
    alert("Hello to you");
};

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
