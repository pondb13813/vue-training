export default {
    namespaced: true,
    state: () => ({
        username: "",
        password: "",
    }),
    getters: {
        whatIsRootTitle(state, getters, rootState) {
            return `Title is ${rootState.title}`;
        },
    },
    mutations: {
        SET_USERNAME(state, val) {
            state.username = val;
        },
        SET_PASSWORD(state, val) {
            state.password = val;
        },
    },
    actions: {
        doSomething({ commit }) {
            // dispatch("fetchTitle", null, { root: true });
            commit("SET_TITLE", "", { root: true });
        },
    },
};
