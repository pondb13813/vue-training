import Vue from "vue";
import Vuex from "vuex";
import user from "./user";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        title: "ROOT_TITLE",
    },
    getters: {
        sayHelloToUser(state) {
            return `Hello to ${state.user.username?.toUpperCase()}`;
        },
    },
    mutations: {
        SET_TITLE(state, val) {
            state.title = val;
        },
    },
    actions: {
        async fetchTitle({ commit }) {
            let newTitle = "CHANGED_TITLE";
            commit("SET_TITLE", newTitle);

            // dispatch('user/doSomething');
        },
    },
    modules: {
        user,
    },
});
